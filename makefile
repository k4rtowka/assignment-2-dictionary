ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm

dict.o: dict.asm lib.o

main.o: main.asm dict.o lib.o words.inc

findword: dict.o lib.o main.o
	$(LD) -o findword dict.o lib.o main.o

all: findword test clean
	
.PHONY: clean, test
clean:
	rm -f *.o

test:
	python3 test.py

