%include "dict.inc"
%include "lib.inc"
%include "words.inc"

global _start

%define bufferSize 256
%define DQ_SIZE 8

section .bss
buffer: resb 256

section .rodata
tooMuchError: db 'Max lenght 256', 0
notFoundError: db 'Word not found, 404', 0

section .text

_start:
    mov rdi, buffer
    mov rsi, bufferSize
    push rdi
    call read_word
    pop rdi
    test rax, rax
    jz .toomuch
    mov rsi, first
    call find_word
    test rax, rax
    jz .notfound
    mov rdi, rax
    add rdi, DQ_SIZE
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call exit
    
.notfound:
    mov rdi, notFoundError
    jmp .error
.toomuch:
    mov rdi, tooMuchError
.error:
    push rdi 
    call string_length 
    pop rsi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    call exit
