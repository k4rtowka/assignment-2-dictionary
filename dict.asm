%include "lib.inc"
global find_word

%define DQ_SIZE 8

section .text

; if notFound return 0, else return start address (rax) 
find_word:
    mov r8, rsi
    mov r9, rdi

.loop:
    add rsi, DQ_SIZE
    call string_equals
    test rax, rax       
    jnz .successful
    mov rsi, qword [r8]
    test rsi, rsi
    jz .notFound
 	mov r8, rsi
	mov rdi, r9
	jmp .loop



.notFound:
    xor rax, rax
    ret 

.successful:
    mov rax, r8
    ret       
