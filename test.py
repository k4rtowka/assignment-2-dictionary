import subprocess
input_str = ["", "first/word", "lolkek", "third+word", "lolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkeklolkek"]
output_str = ["", "first word explanation", "",  "third word explanation", ""]
errput_str = ["Word not found, 404", "", "Word not found, 404", "", "Max lenght 256"]

for i in range(len(input_str)):
    task = subprocess.Popen(["./findword"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = task.communicate(input=input_str[i].encode())
    str_output = stdout.decode().strip()
    str_errput = stderr.decode().strip()
    if str_output == output_str[i] and str_errput == errput_str[i]:
        print('Test '+str(i+1)+' passed. input {stdin: "'+input_str[i]+'"} output {strout: "'+str_output+'", strerr: "'+str_errput+'"}')
    else:
        print("Test "+str(i+1)+' failed. input {stdin: "'+input_str[i]+'"} output {strout: "'+str_output+'", strerr: "'+str_errput+'"}')

